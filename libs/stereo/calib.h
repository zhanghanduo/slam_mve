#ifndef CALIB_HPP
#define CALIB_HPP
#include <iostream>
#include <istream>
#include <fstream>      // std::ifstream
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iomanip>
#include "math/matrix.h"
//#include "so3.hpp"
//----------------------------------------------------------------------------//
//   Calibration Results Storage Class                                        //
//----------------------------------------------------------------------------//
void load_camera_calibration( CvSize &image_size,
                                    std::vector<double> &K,
                                    std::vector<double> &d,
                                    std::istream &in_file );
class Stereo_Calib_Result
{
public:
   Stereo_Calib_Result( void )
      : left_K(9),
        right_K(9),
        left_d(5),
        right_d(5),
        R(9),
        T(3),
        F(9),
    left_R(9),
    right_R(9),
    right_P(12),
    left_P(12),
     left_P_weird(12),
        right_P_weird(12),
    rect_K(9),
    dispToDepth(16)
   {

   }

   void print( std::ostream &out );

   std::vector<double> left_K;   // Left camera intrinsic matrix
   std::vector<double> right_K;  // Right camera intrinsic matrix
   std::vector<double> left_d;   // Left camera distortion coefficients
   std::vector<double> right_d;  // Right camera distortion coefficients
   std::vector<double> R;        // Rotation matrix
   std::vector<double> T;        // Translation std::vector
   std::vector<double> F;        // Fundamental matrix

  std::vector<double> left_R;        // Left Rect Matrix
  std::vector<double> right_R;        // Right Rect Matrix
  std::vector<double> right_P;        // Right Projection Matrix
  std::vector<double> left_P;        // Left Projection Matrix
  std::vector<double> left_P_weird;        // Left Projection Matrix
  std::vector<double> right_P_weird;        // Left Projection Matrix

  std::vector<double> rect_K;        // Recified inttrisic matrix
  std::vector<double> dispToDepth;        // disparity-to-depth mapping matrix, 4x4



};
class PinholeCameraModel
{
public:

  PinholeCameraModel(const cv::Mat &D, const cv::Mat &R, const cv::Mat &K, const cv::Mat &P) : D_(D),R_(R),K_(K),P_(P){
      k_matrix.fill(0.0);
      k_matrix[0] = fx(); k_matrix[2] = cx();
      k_matrix[4] = fy(); k_matrix[5] = cy();
      k_matrix[8] = 1.0;
  }

  PinholeCameraModel(const PinholeCameraModel& other);

  PinholeCameraModel& operator=(const PinholeCameraModel& other);


  /**
   * \brief Returns the original camera matrix.
   */
  const cv::Matx33d& intrinsicMatrix() const;
  /**
   * \brief Project a 3d point to rectified pixel coordinates.
   *
   * This is the inverse of projectPixelTo3dRay().
   *
   * \param xyz 3d point in the camera coordinate frame
   * \return (u,v) in rectified pixel coordinates
   */
  cv::Point2d project3dToPixel(const cv::Point3d& xyz) const;

  /**
   * \brief Project a rectified pixel to a 3d ray.
   *
   * Returns the unit vector in the camera coordinate frame in the direction of rectified
   * pixel (u,v) in the image plane. This is the inverse of project3dToPixel().
   *
   * In 1.4.x, the vector has z = 1.0. Previously, this function returned a unit vector.
   *
   * \param uv_rect Rectified pixel coordinates
   * \return 3d ray passing through (u,v)
   */
  cv::Point3d projectPixelTo3dRay(const cv::Point2d& uv_rect) const;


  /**
   * \brief Compute the rectified image coordinates of a pixel in the raw image.
   */
  cv::Point2d rectifyPoint(const cv::Point2d& uv_raw) const;

  /**
   * \brief Compute the raw image coordinates of a pixel in the rectified image.
   */
  cv::Point2d unrectifyPoint(const cv::Point2d& uv_rect,cv::Mat T=cv::Mat()) const;



  /**
   * \brief Returns the distortion coefficients.
   */
  const cv::Mat_<double>& distortionCoeffs() const;

  /**
   * \brief Returns the rotation matrix.
   */
  const cv::Matx33d& rotationMatrix() const;

  /**
   * \brief Returns the projection matrix.
   */
  const cv::Matx34d& projectionMatrix() const;

  /**
   * \brief Returns the original camera matrix for full resolution.
   */
  const cv::Matx33d& fullIntrinsicMatrix() const;

  /**
   * \brief Returns the projection matrix for full resolution.
   */
  const cv::Matx34d& fullProjectionMatrix() const;

  /**
   * \brief Returns the focal length (pixels) in x direction of the rectified image.
   */
  double fx() const;

  /**
   * \brief Returns the focal length (pixels) in y direction of the rectified image.
   */
  double fy() const;

  /**
   * \brief Returns the x coordinate of the optical center.
   */
  double cx() const;

  /**
   * \brief Returns the y coordinate of the optical center.
   */
  double cy() const;

  /**
   * \brief Returns the x-translation term of the projection matrix.
   */
  double Tx() const;

  /**
   * \brief Returns the y-translation term of the projection matrix.
   */
  double Ty() const;

  /**
   * \brief Returns the number of columns in each bin.
   */
  uint32_t binningX() const;

  /**
   * \brief Returns the number of rows in each bin.
   */
  uint32_t binningY() const;

  /**
   * \brief Compute delta u, given Z and delta X in Cartesian space.
   *
   * For given Z, this is the inverse of getDeltaX().
   *
   * \param deltaX Delta X, in Cartesian space
   * \param Z      Z (depth), in Cartesian space
   */
  double getDeltaU(double deltaX, double Z) const;

  /**
   * \brief Compute delta v, given Z and delta Y in Cartesian space.
   *
   * For given Z, this is the inverse of getDeltaY().
   *
   * \param deltaY Delta Y, in Cartesian space
   * \param Z      Z (depth), in Cartesian space
   */
  double getDeltaV(double deltaY, double Z) const;

  /**
   * \brief Compute delta X, given Z in Cartesian space and delta u in pixels.
   *
   * For given Z, this is the inverse of getDeltaU().
   *
   * \param deltaU Delta u, in pixels
   * \param Z      Z (depth), in Cartesian space
   */
  double getDeltaX(double deltaU, double Z) const;

  /**
   * \brief Compute delta Y, given Z in Cartesian space and delta v in pixels.
   *
   * For given Z, this is the inverse of getDeltaV().
   *
   * \param deltaV Delta v, in pixels
   * \param Z      Z (depth), in Cartesian space
   */
  double getDeltaY(double deltaV, double Z) const;

  /**
   * \brief Returns true if the camera has been initialized
   */
    math::Matrix<double, 3, 3> k_matrix;
protected:
  cv::Mat_<double> D_;           // Unaffected by binning, ROI
  cv::Matx33d R_;           // Unaffected by binning, ROI
  cv::Matx33d K_;           // Describe current image (includes binning, ROI)
  cv::Matx34d P_;           // Describe current image (includes binning, ROI)

};



inline const cv::Matx33d& PinholeCameraModel::intrinsicMatrix() const  { return K_; }
inline const cv::Mat_<double>& PinholeCameraModel::distortionCoeffs() const { return D_; }
inline const cv::Matx33d& PinholeCameraModel::rotationMatrix() const   { return R_; }
inline const cv::Matx34d& PinholeCameraModel::projectionMatrix() const { return P_; }


inline double PinholeCameraModel::fx() const { return P_(0,0); }
inline double PinholeCameraModel::fy() const { return P_(1,1); }
inline double PinholeCameraModel::cx() const { return P_(0,2); }
inline double PinholeCameraModel::cy() const { return P_(1,2); }
inline double PinholeCameraModel::Tx() const { return P_(0,3); }
inline double PinholeCameraModel::Ty() const { return P_(1,3); }

class Stereo_Calib_CV :  public Stereo_Calib_Result{
public:
//  Stereo_Calib_CV(): Stereo_Calib_Result() {}


  void load_from_file(std::istream &in_file){


    //std::ifstream in_file(filename.c_str());

   // Number of cameras
    int num_cams;
    in_file >>num_cams;

   // Left camera intrinsic parameters
   load_camera_calibration( image_size, left_K, left_d, in_file );

   // Left camera R,T at the origin
   int dummy;
   for(int i=0; i<12; i++)
     in_file >> dummy;//"1 0 0\n0 1 0\n0 0 1\n\n0 0 0\n" << std::endl;

   // Right camera intrinsic parameters
   load_camera_calibration( image_size, right_K, right_d, in_file );

   // Right camera rotation matrix
   in_file
            >> R[0]  >> R[1]  >> R[2]
            >> R[3]  >> R[4]  >> R[5]
            >> R[6]  >> R[7]  >> R[8];


   // Right camera translation vector
   in_file
     >> T[0]  >> T[1]  >> T[2] ;

   // Intrnsic rectified camera params

   in_file
            >> left_R[0] >> left_R[1]
        >> left_R[2]

        >> left_R[3]  >> left_R[4]
        >> left_R[5]

        >> left_R[6]  	    >> left_R[7]
        >> left_R[8]
            ;

   in_file
            >> right_R[0]  >> right_R[1]
        >> right_R[2]

        >> right_R[3]  >> right_R[4]
        >> right_R[5]

        >> right_R[6]  	    >> right_R[7]
        >> right_R[8]
            ;

   in_file
        >> rect_K[0]  >> rect_K[1]
        >> rect_K[2]

        >> rect_K[3]  >> rect_K[4]
        >> rect_K[5]

        >> rect_K[6]  	    >> rect_K[7]
        >> rect_K[8]
        ;


     in_file
          >> dispToDepth[0]  >> dispToDepth[1]
          >> dispToDepth[2] >>dispToDepth[3]

          >> dispToDepth[4]  >> dispToDepth[5]
          >> dispToDepth[6] >>dispToDepth[7]

          >> dispToDepth[8]  >> dispToDepth[9]
          >> dispToDepth[10]  >>dispToDepth[11]

          >> dispToDepth[12]  >> dispToDepth[13]
          >> dispToDepth[14] >>  dispToDepth[15];

    left_K_mat  = cv::Mat( 3, 3, CV_64F, &left_K[0]  );
    right_K_mat = cv::Mat( 3, 3, CV_64F, &right_K[0] );
    left_d_mat  = cv::Mat( 1, 5, CV_64F, &left_d[0]  );
    right_d_mat = cv::Mat( 1, 5, CV_64F, &right_d[0] );
    R_mat = cv::Mat( 3, 3, CV_64F, &R[0] );
    T_mat = cv::Mat( 3, 1, CV_64F, &T[0] );
    F_mat = cv::Mat( 3, 3, CV_64F, &F[0] );

    R_left = cv::Mat( 3, 3, CV_64F, &left_R[0] );
    R_right = cv::Mat( 3, 3, CV_64F, &right_R[0] );
    P_left   = cv::Mat(3, 4, CV_64F,  &left_P[0] );
    P_right  = cv::Mat(3, 4, CV_64F,  &right_P[0] );

    P_left_weird   = cv::Mat(3, 4, CV_64F,  &left_P_weird[0] );
    P_right_weird  = cv::Mat(3, 4, CV_64F,  &right_P_weird[0] );
   // rectified K matrix
    Krect  = cv::Mat(3,3,CV_64F,&rect_K[0]);
    Q  = cv::Mat(4,4,CV_64F,&dispToDepth[0]);

   P_left_weird = cv::Mat::eye(3, 4, CV_64FC1 );

   P_right_weird  = ( cv::Mat_<double>(3, 4) <<
         R_mat(0, 0), R_mat(0, 1), R_mat(0, 2), T_mat(0),
         R_mat(1, 0), R_mat(1, 1), R_mat(1, 2), T_mat(1),
         R_mat(2, 0), R_mat(2, 1), R_mat(2, 2), T_mat(2));
   is_vertical = fabs(P_right.at<double>(1, 3)) > fabs(P_right.at<double>(0, 3));
 //  std::cout << R_left << "sdas  " <<P_left<<std::endl;

  int flags = 0;
   flags |= CV_CALIB_ZERO_DISPARITY;
   cv::stereoRectify(left_K_mat,  left_d_mat,right_K_mat, right_d_mat,
            image_size, R_mat, T_mat,
            R_left, R_right, P_left, P_right, Q, flags);
   leftCamRect=new PinholeCameraModel(left_d_mat,R_left,left_K_mat,P_left);
   rightCamRect=new PinholeCameraModel(right_d_mat,R_right,right_K_mat,P_right);


   //After rect only translation on x axis remains
   memset(x_21mu,0,sizeof(x_21mu));
   x_21mu[0] = T_mat(0);
  // print(std::cout);
   initUndistortRectifyMap(left_K_mat,left_d_mat,R_left,Krect,image_size,
                           CV_32FC1,map_leftx,map_lefty);


   initUndistortRectifyMap(right_K_mat,right_d_mat,R_right,Krect,image_size,
                           CV_32FC1,map_rightx,map_righty);
  }

  void rect_images(const cv::Mat &im1_master, cv::Mat &rect_master,const cv::Mat &im1_slave, cv::Mat &rect_slave) const{
      if(image_size.height != im1_master.rows || image_size.width != im1_master.cols ){
          std::cerr << "Calib has incorrect image size for this image\n" << std::endl;
          exit(-1);
      }


                cv::remap(im1_master, rect_master, map_leftx, map_lefty, cv::INTER_LINEAR, cv::BORDER_CONSTANT, cv::Scalar(0));
                cv::remap(im1_slave, rect_slave, map_rightx, map_righty, cv::INTER_LINEAR, cv::BORDER_CONSTANT, cv::Scalar(0));


  }
  //----------------------------------------------------------------------------//
  //   Calibration Results Output                                               //
  //----------------------------------------------------------------------------//

  static void save_camera_calibration( const CvSize &image_size,
                                       const std::vector<double> &K,
                                       const std::vector<double> &d,
                                       std::ostream &out_file )
  {
     // Image size
     out_file << image_size.width << " " << image_size.height << std::endl
              << std::endl;

     // Intrinsic matrix
     out_file << std::setprecision(16)
              << K[0] << " " << K[1] << " " << K[2] << std::endl
              << K[3] << " " << K[4] << " " << K[5] << std::endl
              << K[6] << " " << K[7] << " " << K[8] << std::endl
              << std::endl;

     // Distortion parameters
     out_file << std::setprecision(16)
              << d[0] << " " << d[1] << " " << d[2] << " "
              << d[3] << " " << d[4] << std::endl
              << std::endl;
  }


  void save_stereo_calibration(std::ostream &out_file ) const
  {
     // Number of cameras
     out_file << "2" << std::endl << std::endl;

     // Left camera intrinsic parameters
     save_camera_calibration( image_size, left_K, left_d, out_file );

     // Left camera R,T at the origin
     out_file << "1 0 0\n0 1 0\n0 0 1\n\n0 0 0\n" << std::endl;

     // Right camera intrinsic parameters
     save_camera_calibration( image_size, right_K, right_d, out_file );

     // Right camera rotation matrix
     out_file << std::setprecision(16)
              << R[0] << " " << R[1] << " " << R[2] << std::endl
              << R[3] << " " << R[4] << " " << R[5] << std::endl
              << R[6] << " " << R[7] << " " << R[8] << std::endl
              << std::endl;

     // Right camera translation vector
     out_file << std::setprecision(16)
              << T[0] << " " << T[1] << " " << T[2] << std::endl <<std::endl;

     // Intrnsic rectified camera params

     out_file << std::setprecision(16)
              << left_R[0] << " " << left_R[1] << " "
          << left_R[2] << std::endl

          << left_R[3] << " " << left_R[4] << " "
          << left_R[5] << std::endl

          << left_R[6] << " " 	    << left_R[7] << " "
          << left_R[8] << std::endl
              << std::endl;

     out_file << std::setprecision(16)
              << right_R[0] << " " << right_R[1] << " "
          << right_R[2] << std::endl

          << right_R[3] << " " << right_R[4] << " "
          << right_R[5] << std::endl

          << right_R[6] << " " 	    << right_R[7] << " "
          << right_R[8] << std::endl
              << std::endl;

     out_file << std::setprecision(16)
          << rect_K[0] << " " << rect_K[1] << " "
          << rect_K[2] << std::endl

          << rect_K[3] << " " << rect_K[4] << " "
          << rect_K[5] << std::endl

          << rect_K[6] << " " 	    << rect_K[7] << " "
          << rect_K[8] << std::endl
          << std::endl;


       out_file << std::setprecision(16)
            << dispToDepth[0] << " " << dispToDepth[1] << " "
            << dispToDepth[2] << " "<<dispToDepth[3] <<std::endl

            << dispToDepth[4] << " " << dispToDepth[5] << " "
            << dispToDepth[6] << " "<<dispToDepth[7] <<std::endl

            << dispToDepth[8] << " " << dispToDepth[9] << " "
            << dispToDepth[10] << " " <<dispToDepth[11] <<std::endl

            << dispToDepth[12] << " " << dispToDepth[13] << " "
            << dispToDepth[14] <<  " "<<dispToDepth[15] <<std::endl;

  }

  cv::Mat map_leftx,map_lefty;
  cv::Mat map_rightx,map_righty;

  PinholeCameraModel *leftCamRect,*rightCamRect;
  cv::Mat left_K_mat ;
  cv::Mat right_K_mat;
  cv::Mat left_d_mat ;
  cv::Mat right_d_mat;
  cv::Mat_<double> R_mat ;
  cv::Mat_<double> T_mat ;
  cv::Mat F_mat;

  cv::Mat R_left;
  cv::Mat R_right;
  cv::Mat P_left_weird;
  cv::Mat P_right_weird;


  cv::Mat P_left;
  cv::Mat P_right;
   // rectified K matrix
  cv::Mat Krect;
  cv::Mat Q;
  CvSize image_size;
  bool is_vertical;
  double   x_21mu[6];


};


#endif // CALIB_HPP
