#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <cstring>
#include <cstdlib>
#include "stereo/calib.h"
#include "stereo/stereo_common.h"
#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
namespace plotting_utils{

void convert_to_dmatch(sfm::Matching::Result &matching_result,std::vector<cv::DMatch> matches);


void drawPair2PairMatches(cv::Mat &outimg,mve::ByteImage::Ptr im1_master_bi,mve::ByteImage::Ptr im1_slave_bi,
                          mve::ByteImage::Ptr im2_master_bi,
                          mve::ByteImage::Ptr im2_slave_bi,
                          const sfm::bundler::stereo::StereoViewport &view1,const sfm::bundler::stereo::StereoViewport &view2,
                          const sfm::Matching::Result &matching_result,
                          const Stereo_Calib_CV &calib_file,bool redistort=true);
void drawMatches(cv::Mat &outimg,
                 const sfm::bundler::stereo::StereoViewport &view1,
                 const sfm::Matching::Result &matching_result,
                 bool redistort);
}
