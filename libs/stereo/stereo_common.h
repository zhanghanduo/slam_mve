/*
 * Copyright (C) 2015, Simon Fuhrmann
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#ifndef STEREO_COMMON_HEADER
#define STEREO_COMMON_HEADER

#include <string>
#include <vector>
#include <map>

#include "math/vector.h"
#include "util/aligned_memory.h"
#include "mve/image.h"
#include "sfm/camera_pose.h"
#include "sfm/correspondence.h"
#include "sfm/feature_set.h"
#include "sfm/sift.h"
#include "sfm/surf.h"
#include "sfm/defines.h"
#include "sfm/bundler_common.h"
#include "stereo/featureset_extended.h"
#include "stereo/defines.h"
#include "calib.h"

SFM_NAMESPACE_BEGIN
SFM_BUNDLER_NAMESPACE_BEGIN
STEREO_NAMESPACE_BEGIN
/* -------------------- Common Data Structures -------------------- */
struct StereoTwoViewMatching : public sfm::bundler::TwoViewMatching
{
    math::Matrix<double, 3, 4> relpose;
};
struct StereoPairwiseMatching{
     std::vector<StereoTwoViewMatching> tvms;
     std::map<std::pair<int,int>, int> view_ids2_tvms_idx;
};



/**
 * Per-viewport information.
 * Not all data is required for every step. It should be populated on demand
 * and cleaned as early as possible to keep memory consumption in bounds.
 */
struct StereoViewport : public Viewport
{
    StereoViewport (void);
  /*  StereoViewport( const StereoViewport& other ) :
         image_slave( other.image_slave ), features_slave( other.features_slave ),
         matches_master_slave( other.matches_master_slave ),stereo_calibration(nullptr),pts_3d(other.pts_3d),added(other.added)
      { if(other.stereo_calibration)
        {std::ostringstream strm;
        other.stereo_calibration.save_stereo_calibration(strm);
        std::istringstream istrm(strm.str());

        stereo_calibration = new Stereo_Calib_CV(istrm);
        }
      }*/

    /** The actual image data for debugging purposes. Usually nullptr! */
    mve::ByteImage::Ptr image_slave;
    FeatureSetExtended features;

    /** Per-feature information. */
    FeatureSetExtended features_slave;
    std::vector<int> matches_master_slave;
    Stereo_Calib_CV stereo_calibration;
    std::vector<math::Vec3f> pts_3d;
    bool is_added() const { return added;}
    void set_added(bool added){ this->added=added;}
    bool is_slave(void) const {return slave;}
    bool added;
    bool slave;
};

/** The list of all viewports considered for bundling. */
typedef std::vector<StereoViewport> StereoViewportList;

/* --------------- Data Structure for Feature Tracks -------------- */



/* ------------------ Input/Output for Prebundle ------------------ */

/**
 * Saves the pre-bundle data to file, which records all viewport and
 * matching data necessary for incremental structure-from-motion.
 */
void
save_prebundle_to_file (StereoViewportList const& viewports,
    StereoPairwiseMatching const& matching, std::string const& filename);

/**
 * Loads the pre-bundle data from file, initializing viewports and matching.
 */
void
load_prebundle_from_file (std::string const& filename,
    StereoViewportList* viewports, StereoPairwiseMatching* matching);

/* ------------------------ Implementation ------------------------ */


inline
StereoViewport::StereoViewport (void)
    : Viewport(),added(false),slave(false)
{
}
template <typename T, int N, int M>
inline void
roundprint (math::Matrix<T,N,M> const& m)
{
    for (int i = 0; i < m.rows(); ++i)
        for (int j = 0; j < m.cols(); ++j)
            std::cout << (m(i,j) > 0.0 ? floor(m(i,j)*10.0)/10.0 :ceil(m(i,j)*10.0)/10.0) << (j == m.cols() - 1 ? "\n" : " ");
    std::cout << std::endl;
}

template <typename T, int N>
inline void
roundprint (math::Vector<T,N> const& v)
{
    for (int i = 0; i < N - 1; ++i)
         std::cout << floor(v[i]*10.0)/10.0 << " ";
    std::cout << floor(v[N-1]*10.0)/10.0 << std::endl;
}
STEREO_NAMESPACE_END
SFM_NAMESPACE_END
SFM_BUNDLER_NAMESPACE_END
#endif /* STEREO_COMMON_HEADER */
