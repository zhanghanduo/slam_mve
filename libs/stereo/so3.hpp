#pragma once

#include <math.h>

#include "simple_linalg.hpp"



namespace math {

namespace so3 {

/**
 * R: array of column-major 3x3 rotation matrix (ie, from an Eigen matrix)
 * rph: length-3 array of roll, pitch, yaw
 */
template <typename T>
void
rot2rph (const T R[9], T rph[3])
{
#define R(i,j) (R[j*3+i])
    // heading
    rph[2] = atan2 (R(1,0), R(0,0));
    T sh, ch;
    sh = sin (rph[2]);
    ch = cos (rph[2]);

    // pitch
    rph[1] = atan2 (-R(2,0), R(0,0)*ch + R(1,0)*sh);

    // roll
    rph[0] = atan2 (R(0,2)*sh - R(1,2)*ch, -R(0,1)*sh + R(1,1)*ch);
#undef R
}

/**
 * rph: length-3 array of roll, pitch, yaw
 * R: array of column-major 3x3 rotation matrix (ie, from an Eigen matrix)
 */
template <typename T>
void
rotxyz (const T rph[3], T R[9])
{
#define R(i,j) (R[j*3+i])
    T sr, sp, sh, cr, cp, ch;
    sr = sin (rph[0]);
    sp = sin (rph[1]);
    sh = sin (rph[2]);
    cr = cos (rph[0]);
    cp = cos (rph[1]);
    ch = cos (rph[2]);

    R(0,0) = ch*cp; R(0,1) = -sh*cr + ch*sp*sr; R(0,2) = sh*sr + ch*sp*cr;
    R(1,0) = sh*cp; R(1,1) = ch*cr + sh*sp*sr; R(1,2) = -ch*sr + sh*sp*cr;
    R(2,0) = -sp; R(2,1) = cp*sr; R(2,2) = cp*cr;
#undef R
}

template <typename T>
void
axis_angle (const T n1[3], const T n2[3], T axis[3], T &angle)
{
    simple_linalg::cross_product (n1, n2, axis);
    double axisnorm = simple_linalg::norm_3x1 (axis);
    if (fabs (axisnorm) > 1e-6) {
        axis[0] /= axisnorm;
        axis[1] /= axisnorm;
        axis[2] /= axisnorm;
    }
    
    angle = acos (simple_linalg::dot_3x1 (n1, n2) / 
                  simple_linalg::norm_3x1 (n1) * simple_linalg::norm_3x1 (n2));
}

template <typename T>
void
axis_angle_to_rotation_mat (const T axis[3], const T angle, T R[9])
{
#define R(i,j) (R[j*3+i])
    T theta = angle;

    R(0,0) = cos (theta) + (axis[0]*axis[0]) * (1 - cos (theta));
    R(0,1) = axis[0]*axis[1]*(1-cos (theta)) - axis[2]*sin (theta);
    R(0,2) = axis[0]*axis[2]*(1-cos (theta)) + axis[1]*sin (theta);

    R(1,0) = axis[1]*axis[0]*(1-cos (theta)) + axis[2]*sin (theta);
    R(1,1) = cos (theta) + (axis[1]*axis[1]) * (1 - cos (theta));
    R(1,2) = axis[1]*axis[2]*(1-cos (theta)) - axis[0]*sin (theta);

    R(2,0) = axis[2]*axis[0]*(1-cos (theta)) - axis[1]*sin (theta);
    R(2,1) = axis[2]*axis[1]*(1-cos (theta)) + axis[0]*sin (theta);
    R(2,2) = cos (theta) + (axis[2]*axis[2])*(1-cos (theta));
#undef R
}

}

}


