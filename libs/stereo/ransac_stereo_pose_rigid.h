/*
 * Copyright (C) 2015, Simon Fuhrmann
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#ifndef STEREO_RANSAC_POSE_P3P_HEADER
#define STEREO_RANSAC_POSE_P3P_HEADER

#include <vector>

#include "math/matrix.h"
#include "math/vector.h"
#include "sfm/correspondence.h"
#include "sfm/defines.h"
#include "stereo/defines.h"
#include "stereo/stereo_features.h"
#include "sfm/ransac_pose_p3p.h"
SFM_NAMESPACE_BEGIN
SFM_BUNDLER_NAMESPACE_BEGIN
STEREO_NAMESPACE_BEGIN
/**
 * RANSAC pose estimation from 2D-3D correspondences and known camera
 * calibration using the perspective 3-point (P3P) algorithm.
 *
 * The rotation and translation of a camera is determined from a set of
 * 2D image to 3D point correspondences contaminated with outliers. The
 * algorithm iteratively selects 3 random correspondences and returns the
 * result which led to the most inliers.
 *
 * The input 2D image coordinates, the input K-matrix and the threshold
 * in the options must be consistent. For example, if the 2D image coordinates
 * are NOT normalized but in pixel coordinates, the K-matrix must be in pixel
 * coordinates and the threshold must be in pixel, too. For this algorithm,
 * image coordinates do not need to be normalized for stability.
 */
class RansacPoseRigidStereo : public RansacPoseP3P
{
public:
                                         struct Options : public RansacPoseP3P::Options
                                          {
                                              Options (void);


                                              /**
                                               * Threshold used to determine inliers. Defaults to 0.005.
                                               * This threshold assumes that the input points are normalized.
                                               */
                                              double rigid_threshold;

                                          };
public:
    explicit RansacPoseRigidStereo (Options const& options);

    void estimate (StereoMatchCorrespondences2D3D const& corresp,
        math::Matrix<double, 3, 3> const& k_matrix, double tx,
        Result* result);

private:
    typedef math::Matrix<double, 3, 4> Pose;
    typedef std::vector<Pose> PutativePoses;

private:
    void compute_rigid (StereoMatchCorrespondences2D3D const& corresp,
       Pose &pose);

    void find_inliers (StereoMatchCorrespondences2D3D const& corresp,
                       math::Matrix<double, 3, 3> const& k_matrix, double tx,
                       Pose const& pose, std::vector<int>* inliers);

private:
    Options opts;
};

    /* ------------------------ Implementation ------------------------ */

    inline
    RansacPoseRigidStereo::Options::Options() : RansacPoseP3P::Options::Options ()
        , rigid_threshold(2.0)
    {
    }
STEREO_NAMESPACE_END
SFM_BUNDLER_NAMESPACE_END
SFM_NAMESPACE_END

#endif /* STEREO_RANSAC_POSE_P3P_HEADER */
