#include "single_view_stereo.h"
MVS_NAMESPACE_BEGIN
STEREO_NAMESPACE_BEGIN

void
SingleViewStereo::loadColorImage(int _minLevel)
{
    sv->loadColorImage(_minLevel);
    img_pyramid_slave = ImagePyramidCache::get(scene, view, embedding_slave, _minLevel);

}
void SingleViewStereo::prepareMasterView(int scale){
    sv->prepareMasterView(scale);
    this->target_level_slave = (*this->img_pyramid_slave)[scale];
    this->has_target_level_slave = true;

}

MVS_NAMESPACE_END
STEREO_NAMESPACE_END
