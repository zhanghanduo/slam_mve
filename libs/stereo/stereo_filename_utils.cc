#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include "stereo_filename_utils.h"
std::string trim(std::string const& str)
{
    if(str.empty())
        return str;

    std::size_t firstScan = str.find_first_not_of(' ');
    std::size_t first     = firstScan == std::string::npos ? str.length() : firstScan;
    std::size_t last      = str.find_last_not_of(' ');
    return str.substr(first, last-first+1);
}

bool ExpandWildCard(util::fs::Directory& names, util::fs::Directory& result, std::string const& substr) {
  auto oldsize = result.size();
  std::copy_if(std::begin(names), std::end(names),
    std::back_inserter(result),
    [&](util::fs::File const& file) {
      return file.name.find(substr)!=std::string::npos;
    }
  );

  return (result.size() > oldsize);
}

void ReplaceStringInPlace(std::string& subject, const std::string& search,
                          const std::string& replace) {
    size_t pos = 0;
    while ((pos = subject.find(search, pos)) != std::string::npos) {
         subject.replace(pos, search.length(), replace);
         pos += replace.length();
    }
}
void RemoveNonPairs(util::fs::Directory& master, util::fs::Directory& slave,  std::string const& master_substr,
                    std::string const& slave_substr,int trim_name,
                    std::vector<std::pair<util::fs::File,util::fs::File> > &pairs) {

    std::map<std::string,std::vector<util::fs::File> > valid_basenames;
    for(auto f: master){
        std::string fname=f.name;
        ReplaceStringInPlace(fname,master_substr,"");
        if (fname.size () > 0)  fname.resize (fname.size () - trim_name);
        std::vector<util::fs::File>v;
        v.push_back(f);
        valid_basenames[fname]=v;
    }

    for(auto f: slave){
        std::string fname=f.name;
        ReplaceStringInPlace(fname,slave_substr,"");
        if (fname.size () > 0)  fname.resize (fname.size () - trim_name);
        if(valid_basenames.count(fname))
         valid_basenames[fname].push_back(f);
    }
    std::cout << valid_basenames.size() << std::endl;

    for (auto it : valid_basenames){
        if (it.second.size() == 2)
            pairs.push_back(std::make_pair(it.second[0],it.second[1]));
    }
    std::cout << pairs.size() << std::endl;
}
void ReadAllBytes(char const* filename,std::vector<char> &result)
{
    std::ifstream ifs(filename, std::ios::binary|std::ios::ate);
    if(!ifs.good()){
        return;
    }
    std::ifstream::pos_type pos = ifs.tellg();

    result.resize(pos);

    ifs.seekg(0, std::ios::beg);
    ifs.read(&result[0], pos);

}
