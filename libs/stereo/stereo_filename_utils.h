#ifndef STEREO_FILENAME_UTILS_H
#define STEREO_FILENAME_UTILS_H
#include "util/string.h"
#include "util/file_system.h"
#include "util/tokenizer.h"
bool ExpandWildCard(util::fs::Directory& names, util::fs::Directory& result, std::string const& substr);
void RemoveNonPairs(util::fs::Directory& master, util::fs::Directory& slave,  std::string const& master_substr,
                    std::string const& slave_substr,int trim_name,
                    std::vector<std::pair<util::fs::File,util::fs::File> > &pairs);
void ReadAllBytes(char const* filename,std::vector<char> &result);
#endif // STEREO_FILENAME_UTILS_H
