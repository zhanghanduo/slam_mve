/*
 * Copyright (C) 2015, Simon Fuhrmann
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#ifndef STEREO_MATCHING_HEADER
#define STEREO_MATCHING_HEADER

#include <vector>
#include <string>
#include <sstream>
#include "stereo/defines.h"
#include "stereo/stereo_common.h"
#include "stereo/ransac_stereo_pose_rigid.h"
#include "sfm/bundler_matching.h"


SFM_NAMESPACE_BEGIN
SFM_BUNDLER_NAMESPACE_BEGIN
STEREO_NAMESPACE_BEGIN

/**
 * StereoMatching Component: Matching between views in an MVE scene.
 *
 * For every view the feature embedding is loaded and matched to all other
 * views with smaller ID (since the matching is symmetric). Two-view matching
 * involves RANSAC to compute the fundamental matrix (geometric filtering).
 * Only views with a minimum number of matches are considered "connected".
 *
 * The global matching result can be saved to and loaded from file.
 * The file format is a binary sequence of numbers and IDs (all int32_t):
 *
 * <number of results>
 * <view ID 1> <view ID 2> <number of matches>
 *   <match 1 feature ID 1> <match 1 feature ID 2>
 *   <match 2 feature ID 1> <match 2 feature ID 2>
 *   ...
 * <view ID 3> <view ID 4> <number of matches>
 * ...
 *
 * Note:
 * - Only supports SIFT at the moment.
 */


class StereoMatching :public Matching
{

public:

struct Options : public Matching::Options {
    RansacPoseRigidStereo::Options pose_rigid_opts;
    bool verbose_output;
};

public:
    StereoMatching (Options const& options, sfm::bundler::Matching::Progress* progress = nullptr);

    /**
     * Computes the pairwise matching between all stereo pairs of views.
     * Computation requires both descriptor data and 2D feature positions
     * in the viewports.
     */
    void compute (StereoViewportList const& viewports,
        StereoPairwiseMatching* pairwise_matching);
    void
    two_view_matching_internal(StereoViewport const&viewport_1, StereoViewport const&viewport_2, CorrespondenceIndices* matches,math::Matrix<double, 3, 4> &relpose,
        std::stringstream& message,sfm::Matching::Result &matching_result);
private:
    void two_view_matching (StereoViewport const &viewport_1, StereoViewport const&viewport_2, FeatureSetExtended const& view_1, FeatureSetExtended const& view_2,
        CorrespondenceIndices* matches, math::Matrix<double, 3, 4> &relpose,std::stringstream& message);

private:
    Options opts;
    sfm::bundler::Matching::Progress* progress;
};

inline
StereoMatching::StereoMatching (Options const& options, sfm::bundler::Matching::Progress* progress)
    : Matching(options,nullptr),opts(options)
    , progress(progress)
{
}

STEREO_NAMESPACE_END
SFM_BUNDLER_NAMESPACE_END
SFM_NAMESPACE_END

#endif /* SFM_BUNDLER_MATCHING_HEADER */
