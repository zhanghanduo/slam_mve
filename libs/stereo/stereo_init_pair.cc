/*
 * Copyright (C) 2015, Simon Fuhrmann
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#include <iostream>
#include <vector>
#include <sstream>

#include "sfm/ransac_homography.h"
#include "sfm/triangulate.h"
#include "stereo/stereo_init_pair.h"
#include "stereo/defines.h"
SFM_NAMESPACE_BEGIN
SFM_BUNDLER_NAMESPACE_BEGIN
STEREO_NAMESPACE_BEGIN

void
InitialPair::compute_pair (Result* result)
{
    if (this->viewports == NULL || this->tracks == NULL)
        throw std::invalid_argument("NULL viewports or tracks");

    std::cout << "Searching for initial pair..." << std::endl;

    /* Convert tracks to pairwise information. */
    std::vector<CandidatePair> candidates;
    this->compute_candidate_pairs(&candidates);

    /* Sort the candidate pairs by number of matches. */
    std::sort(candidates.rbegin(), candidates.rend());

    /* Search for a good initial pair with few homography inliers. */
    bool found_pair = false;
    std::size_t found_pair_id = std::numeric_limits<std::size_t>::max();
#pragma omp parallel for schedule(dynamic)
    for (std::size_t i = 0; i < candidates.size(); ++i)
    {
        if (found_pair)
            continue;

        /* Reject pairs with 8 or fewer matches. */
        CandidatePair const& candidate = candidates[i];


        /* Compute initial pair pose. */
        CameraPose pose;
        bool const found_pose = this->compute_pose(candidate, &pose);
        if (!found_pose)
        {
            this->debug_output(candidate);
            continue;
        }


#pragma omp critical
        if (i < found_pair_id)
        {
            result->view_id = candidate.view_id;
            result->view_pose = pose;
            found_pair_id = i;
            found_pair = true;
        }
    }

    if (!found_pair)
    {
        result->view_id = -1;
    }
}

void
InitialPair::compute_pair (int view_id, Result* result)
{

    /* Convert tracks to pairwise information. */
    std::vector<CandidatePair> candidates;
    this->compute_candidate_pairs(&candidates);

    /* Find candidate pair. */
    CandidatePair* candidate = NULL;
    for (std::size_t i = 0; candidate == NULL && i < candidates.size(); ++i)
    {
        if (view_id == candidates[i].view_id)

            candidate = &candidates[i];
    }
    if (candidate == NULL)
        throw std::runtime_error("No matches for initial pair");

    /* Compute initial pair pose. */
    result->view_id = view_id;
    bool const found_pose = this->compute_pose(*candidate,
        &result->view_pose);
    if (!found_pose)
        throw std::runtime_error("Cannot compute pose for initial pair");
}

void
InitialPair::compute_candidate_pairs (CandidatePairs* candidates)
{
    /*
     * Convert the tracks to pairwise information. This is similar to using
     * the pairwise matching result directly, however, the tracks have been
     * further filtered during track generation.
     */
    int const num_viewports = static_cast<int>(this->viewports->size());
    std::vector<int> candidate_lookup(MATH_POW2(num_viewports), -1);
    candidates->reserve(1000);
    for (std::size_t i = 0; i < this->tracks->size(); ++i)
    {
        Track const& track = this->tracks->at(i);
        for (std::size_t j = 1; j < track.features.size(); ++j)
            for (std::size_t k = 0; k < j; ++k)
            {
                int v1id = track.features[j].view_id;
                int v2id = track.features[k].view_id;
                int f1id = track.features[j].feature_id;
                int f2id = track.features[k].feature_id;
                if (v1id > v2id)
                {
                    std::swap(v1id, v2id);
                    std::swap(f1id, f2id);
                }

                /* Lookup pair. */
                int const lookup_id = v1id * num_viewports + v2id;
                int pair_id = candidate_lookup[lookup_id];
                if (pair_id == -1)
                {
                    pair_id = static_cast<int>(candidates->size());
                    candidate_lookup[lookup_id] = pair_id;
                    candidates->push_back(CandidatePair());
                    candidates->back().view_id = v1id;
                }

                /* Fill candidate with additional info. */
                StereoViewport const& view1 = this->viewports->at(v1id);
                StereoViewport const& view2 = this->viewports->at(v2id);
                math::Vec2f const v1pos = view1.features.positions[f1id];
                math::Vec2f const v2pos = view2.features.positions[f2id];
                Correspondence2D2D match;
                std::copy(v1pos.begin(), v1pos.end(), match.p1);
                std::copy(v2pos.begin(), v2pos.end(), match.p2);
                candidates->at(pair_id).matches.push_back(match);
            }
    }
}

bool
InitialPair::compute_pose (CandidatePair const& candidate,
    CameraPose* pose)
{


    /* Populate K-matrices. */
    StereoViewport const& view = this->viewports->at(candidate.view_id);
    pose->set_k_matrix(view.focal_length, this->viewports->at(candidate.view_id).stereo_calibration.leftCamRect->cx(), this->viewports->at(candidate.view_id).stereo_calibration.leftCamRect->cy());
    pose->init_canonical_form();

    math::matrix_set_identity(*pose->R, 3);

    pose->t.fill(0.0);
    return true;

}


void
InitialPair::debug_output (CandidatePair const& candidate,
    std::size_t num_inliers, double angle)
{
    if (!this->opts.verbose_output)
        return;

    std::stringstream message;
    std::size_t num_matches = candidate.matches.size();
    message << "  Pair " << std::setw(3) << candidate.view_id
        << ": " << std::setw(4) << num_matches << " matches";

    if (num_inliers > 0)
    {
        float percentage = static_cast<float>(num_inliers) / num_matches;
        message << ", " << std::setw(4) << num_inliers
            << " H-inliers (" << (int)(100.0f * percentage) << "%)";
    }

    if (angle > 0.0)
    {
        message << ", " << std::setw(4)
            << util::string::get_fixed(MATH_RAD2DEG(angle), 2)
            << " pair angle";
    }

#pragma omp critical
    std::cout << message.str() << std::endl;
}
STEREO_NAMESPACE_END
SFM_BUNDLER_NAMESPACE_END
SFM_NAMESPACE_END

