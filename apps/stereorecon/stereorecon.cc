/*
 * Copyright (C) 2015, Simon Fuhrmann
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the GPL 3 license. See the LICENSE.txt file for details.
 */

#include <fstream>
#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
#include <opencv2/highgui/highgui.hpp>
#include "util/system.h"
#include "util/timer.h"
#include "util/arguments.h"
#include "util/file_system.h"
#include "util/tokenizer.h"
#include "mve/scene.h"
#include "mve/bundle.h"
#include "mve/bundle_io.h"
#include "mve/image.h"
#include "mve/image_io.h"
#include "mve/image_tools.h"
#include "sfm/nearest_neighbor.h"
#include "sfm/feature_set.h"
#include "sfm/bundler_common.h"
#include "sfm/bundler_features.h"
#include "sfm/bundler_matching.h"
#include "sfm/bundler_tracks.h"
#include "sfm/bundler_init_pair.h"
#include "sfm/bundler_intrinsics.h"
#include "sfm/bundler_incremental.h"
#include "stereo/stereo_features.h"
#include "stereo/stereo_matching.h"
#include "stereo/stereo_incremental.h"
#include "stereo/stereo_init_pair.h"
#define RAND_SEED_MATCHING 0
#define RAND_SEED_SFM 0

struct AppSettings
{
    std::string scene_path;
    std::string original_name;
    std::string undistorted_name;
    std::string exif_name;
    std::string prebundle_file;
    std::string log_file;
    int max_image_size;
    bool lowres_matching;
    bool normalize_scene;
    bool skip_sfm;
    bool always_full_ba;
    bool fixed_intrinsics;
    bool shared_intrinsics;
    bool intrinsics_from_views;
    bool add_fixed_stereo_views;

    int video_matching;
    float track_error_thres_factor;
    float new_track_error_thres;
    int initial_pair_1;
    int initial_pair_2;
    int min_views_per_track;
    int min_feat_thresh;
    int max_epipolar_distance;
};

void
log_message (AppSettings const& conf, std::string const& message)
{
    if (conf.log_file.empty())
        return;
    std::string fname = util::fs::join_path(conf.scene_path, conf.log_file);
    std::ofstream out(fname.c_str(), std::ios::app);
    if (!out.good())
        return;

    time_t rawtime;
    std::time(&rawtime);
    struct std::tm* timeinfo;
    timeinfo = std::localtime(&rawtime);
    char timestr[20];
    std::strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", timeinfo);

    out << timestr << "  " << message << std::endl;
    out.close();
}
std::string
make_image_name (int id)
{
    return "view_" + util::string::get_filled(id, 4) + ".mve";
}
void
features_and_matching (mve::Scene::Ptr scene, AppSettings const& conf,
    sfm::bundler::stereo::StereoViewportList* viewports,
    sfm::bundler::stereo::StereoPairwiseMatching* pairwise_matching)
{
    /* Feature computation for the scene. */
    sfm::bundler::stereo::StereoFeatures::Options feature_opts;
    feature_opts.image_embedding = conf.original_name;
    feature_opts.max_image_size = conf.max_image_size;
    feature_opts.feature_options.feature_types = sfm::FeatureSetExtended::FEATURE_ALL_EXTENDED;
    feature_opts.feature_options.nFeatures=4000;
    //feature_opts.feature_options.sift_opts.contrast_threshold = 0.0001;
    //feature_opts.feature_options.surf_opts.contrast_threshold = 100.0;


    feature_opts.max_epipolar_distance=conf.max_epipolar_distance;
    std::cout << "Computing image features..." << std::endl;
    {
        util::WallTimer timer;
        sfm::bundler::stereo::StereoFeatures bundler_features(feature_opts);
        bundler_features.compute(scene, viewports);

        std::cout << "Computing features took " << timer.get_elapsed()
            << " ms." << std::endl;
        log_message(conf, "Feature detection took "
            + util::string::get(timer.get_elapsed()) + "ms.");
    }

    /* Exhaustive matching between all pairs of views. */
    sfm::bundler::stereo::StereoMatching::Options matching_opts;
    matching_opts.pose_rigid_opts.max_iterations = 1000;
    matching_opts.pose_rigid_opts.rigid_threshold = conf.max_epipolar_distance;
    matching_opts.min_feature_matches=conf.min_feat_thresh;
    matching_opts.ransac_opts.verbose_output = true;
    matching_opts.use_lowres_matching = conf.lowres_matching;
    matching_opts.match_num_previous_frames = conf.video_matching;

    std::cout << "Performing feature matching..." << std::endl;
    {
        util::WallTimer timer;
        sfm::bundler::stereo::StereoMatching bundler_matching(matching_opts);
        bundler_matching.compute(*viewports, pairwise_matching);
        std::cout << "Matching took " << timer.get_elapsed()
            << " ms." << std::endl;
        log_message(conf, "Feature matching took "
            + util::string::get(timer.get_elapsed()) + "ms.");
    }

    if (pairwise_matching->tvms.empty())
    {
        std::cerr << "Error: No matching image pairs. Exiting." << std::endl;
        std::exit(EXIT_FAILURE);
    }
}

void
sfm_reconstruct (AppSettings const& conf)
{
#if ENABLE_SSE2_NN_SEARCH && defined(__SSE2__)
    std::cout << "SSE2 accelerated matching is enabled." << std::endl;
#else
    std::cout << "SSE2 accelerated matching is disabled." << std::endl;
#endif

#if ENABLE_SSE3_NN_SEARCH && defined(__SSE3__)
    std::cout << "SSE3 accelerated matching is enabled." << std::endl;
#else
    std::cout << "SSE3 accelerated matching is disabled." << std::endl;
#endif

    /* Load scene. */
    mve::Scene::Ptr scene;
    try
    {
        scene = mve::Scene::create(conf.scene_path);
    }
    catch (std::exception& e)
    {
        std::cerr << "Error loading scene: " << e.what() << std::endl;
        std::exit(EXIT_FAILURE);
    }

    /* Try to load the pairwise matching from the prebundle. */
    std::string const prebundle_path
        = util::fs::join_path(scene->get_path(), conf.prebundle_file);
    sfm::bundler::stereo::StereoViewportList viewports;
    sfm::bundler::stereo::StereoPairwiseMatching pairwise_matching;
    if (!util::fs::file_exists(prebundle_path.c_str()))
    {
        log_message(conf, "Starting feature matching.");
        util::system::rand_seed(RAND_SEED_MATCHING);
        features_and_matching(scene, conf, &viewports, &pairwise_matching);
        std::cout << "Saving pre-bundle to file..." << std::endl;
        sfm::bundler::stereo::save_prebundle_to_file(viewports, pairwise_matching, prebundle_path);
    }
    else if (!conf.skip_sfm)
    {
        log_message(conf, "Loading pairwise matching from file.");
        std::cout << "Loading pairwise matching from file..." << std::endl;
        sfm::bundler::stereo::load_prebundle_from_file(prebundle_path,
            &viewports, &pairwise_matching);
    }

    if (conf.skip_sfm)
    {
        std::cout << "Prebundle finished, skipping SfM. Exiting." << std::endl;
        std::exit(EXIT_SUCCESS);
    }

    /* Drop descriptors and embeddings to save memory. */
    /*scene->cache_cleanup();
    for (std::size_t i = 0; i < viewports.size(); ++i)
        viewports[i].features.clear_descriptors();
*/
    /* Check if there are some matching images. */
    if (pairwise_matching.tvms.empty())
    {
        std::cerr << "No matching image pairs. Exiting." << std::endl;
        std::exit(EXIT_FAILURE);
    }

    /*
     * Obtain camera intrinsics from the views or guess them from EXIF.
     * If neither is available, fall back to a default.
     *
     * FIXME: Once width and height in the viewport is gone, this fully
     * initializes the viewports. Thus viewport info does not need to be
     * saved in the prebundle.sfm, and the file becomes matching.sfm.
     * Obtaining EXIF guesses can be moved out of the feature module to here.
     * The following code can become its own module "bundler_intrinsics".
     */
    /*{
        sfm::bundler::Intrinsics::Options intrinsics_opts;
        if (conf.intrinsics_from_views)
        {
            intrinsics_opts.intrinsics_source
                = sfm::bundler::Intrinsics::FROM_VIEWS;
        }
        std::cout << "Initializing camera intrinsics..." << std::endl;
        sfm::bundler::Intrinsics intrinsics(intrinsics_opts);
        intrinsics.compute(scene, &viewports);
    }*/
    sfm::bundler::ViewportList mono_viewports;

    for(auto &viewport : viewports){

        viewport.focal_length = viewport.stereo_calibration.leftCamRect->fx();
        viewport.radial_distortion = 0.0f;
        sfm::bundler::Viewport mono_viewport=viewport;
        mono_viewport.features=viewport.features;

        mono_viewports.push_back(mono_viewport);
    }


    /* Start incremental SfM. */
    log_message(conf, "Starting incremental SfM.");
    util::WallTimer timer;
    util::system::rand_seed(RAND_SEED_SFM);

    /* Compute connected feature components, i.e. feature tracks. */
    sfm::bundler::TrackList tracks;
    {
        sfm::bundler::Tracks::Options tracks_options;
        tracks_options.verbose_output = true;

        sfm::bundler::Tracks bundler_tracks(tracks_options);
        std::cout << "Computing feature tracks..." << std::endl;
        sfm::bundler::PairwiseMatching mono_pairwise_matching;
        for(auto &pair : pairwise_matching.tvms){
           // std::cout <<"Pair " <<pair.view_1_id <<"-"<< pair.view_2_id<<"\n";
            sfm::bundler::TwoViewMatching mono_tvm=pair;
            mono_pairwise_matching.push_back(mono_tvm);
        }
        bundler_tracks.compute(mono_pairwise_matching, &mono_viewports, &tracks);
        std::cout << "Created a total of " << tracks.size()
            << " tracks." << std::endl;
    }


    for(std::size_t i=0; i< mono_viewports.size(); i++){
        viewports[i].track_ids=mono_viewports[i].track_ids;
    }
    /* Remove color data and pairwise matching to save memory. */
    for (std::size_t i = 0; i < viewports.size(); ++i)
        viewports[i].features.colors.clear();

    /* Search for a good initial pair, or use the user-specified one. */
    sfm::bundler::stereo::InitialPair::Result init_pair_result;
    sfm::bundler::stereo::InitialPair::Options init_pair_opts;
    if (conf.initial_pair_1 < 0)
    {
        init_pair_opts.verbose_output = true;

        sfm::bundler::stereo::InitialPair init_pair(init_pair_opts);
        init_pair.initialize(viewports, tracks,pairwise_matching);
        init_pair.compute_pair(&init_pair_result);
    }
    else
    {
        sfm::bundler::stereo::InitialPair init_pair(init_pair_opts);
        init_pair.initialize(viewports, tracks,pairwise_matching);
        init_pair.compute_pair(conf.initial_pair_1,
            &init_pair_result);
    }

    if (init_pair_result.view_id < 0
        || init_pair_result.view_id >= static_cast<int>(viewports.size()))
    {
        std::cerr << "Error finding initial pair, exiting!" << std::endl;
        std::cerr << "Try manually specifying an initial pair." << std::endl;
        std::exit(EXIT_FAILURE);
    }

    std::cout << "Using view " << init_pair_result.view_id
       // << " and " << init_pair_result.view_2_id
        << " as initial frame." << std::endl;

    /* Incrementally compute full bundle. */
    sfm::bundler::stereo::Incremental::Options incremental_opts;
    //incremental_opts.pose_p3p_opts.max_iterations = 1000;
    //incremental_opts.pose_p3p_opts.threshold = 0.005f;
    incremental_opts.pose_p3p_opts.verbose_output = false;
    incremental_opts.track_error_threshold_factor = conf.track_error_thres_factor;
    incremental_opts.new_track_error_threshold = conf.new_track_error_thres;
    incremental_opts.min_triangulation_angle = MATH_DEG2RAD(1.0);
    incremental_opts.ba_fixed_intrinsics = conf.fixed_intrinsics;
    incremental_opts.ba_shared_intrinsics = conf.shared_intrinsics;
    incremental_opts.verbose_output = true;

    /* Initialize viewports with initial pair. */
    viewports[init_pair_result.view_id].added = true;//init_pair_result.view_1_pose;
//    viewports[init_pair_result.view_2_id].added = true; //init_pair_result.view_2_pose;

    viewports[init_pair_result.view_id].pose = init_pair_result.view_pose;
    //viewports[init_pair_result.view_2_id].pose = init_pair_result.view_2_pose;
   // printf("Cam 1 pos ");
    //sfm::bundler::stereo::print_camera_position(viewports[init_pair_result.view_id].pose );

    /* Initialize the incremental bundler and reconstruct first tracks. */
    sfm::bundler::stereo::Incremental incremental(incremental_opts);
    incremental.initialize(&viewports, &tracks,&pairwise_matching);
    //incremental.set_last_view_id(init_pair_result.view_2_id);
    incremental.triangulate_new_tracks();
  //  incremental.invalidate_large_error_tracks( incremental_opts.track_error_threshold_factor );
    incremental.get_total_error();

    /* Run bundle adjustment. */
    //std::cout << "Running full bundle adjustment..." << std::endl;
   // incremental.bundle_adjustment_full();
   // incremental.invalidate_large_error_tracks();

    /* Reconstruct remaining views. */
    int num_cameras_reconstructed = 2;
    int full_ba_num_skipped = 0;
    while (true)
    {
        /* Find suitable next views for reconstruction. */
        std::vector<int> next_views;
        incremental.find_next_views(&next_views);
        if (next_views.empty())
        {
            std::cout << "SfM reconstruction finished." << std::endl;
            break;
        }

        /* Reconstruct the next view. */
        int next_view_id = -1;
        for (std::size_t i = 0; i < next_views.size(); ++i)
        {
            std::cout << std::endl;
            std::cout << "Adding next view ID " << next_views[i]
                << " (" << (num_cameras_reconstructed + 1) << " of "
                << viewports.size() << ")..." << std::endl;
            if (incremental.reconstruct_next_view(next_views[i]))
            {
                //std::cout  << "Added " <<next_views[i] << " OK\n";
                incremental.invalidate_large_error_tracks( incremental_opts.track_error_threshold_factor );

                next_view_id = next_views[i];
                break;
            }
        }

        if (next_view_id < 0)
        {
            std::cout << "No valid next view. Exiting." << std::endl;
            break;
        }

        /* Run single-camera bundle adjustment. */
        std::cout << "Running single camera bundle adjustment..." << std::endl;
        incremental.bundle_adjustment_single_cam(next_view_id);
        incremental.triangulate_new_tracks();
        incremental.invalidate_large_error_tracks(conf.max_epipolar_distance);
        incremental.get_total_error();

        num_cameras_reconstructed += 1;

        /* Run full bundle adjustment only after a couple of views. */
        int const full_ba_skip_views = conf.always_full_ba ? 0
            : std::min(5, num_cameras_reconstructed / 15);
        if (full_ba_num_skipped < full_ba_skip_views)
        {
            std::cout << "Skipping full bundle adjustment (skipping "
                << full_ba_skip_views << " views)." << std::endl;
            full_ba_num_skipped += 1;
        }
        else
        {
            std::cout << "Running full bundle adjustment..." << std::endl;
            incremental.bundle_adjustment_full();
            full_ba_num_skipped = 0;
        }
    }

    if (full_ba_num_skipped > 0)
    {
        std::cout << "Running final bundle adjustment..." << std::endl;
        incremental.bundle_adjustment_full();
    }

    incremental.get_total_error();

    if(conf.add_fixed_stereo_views)
        incremental.add_fixed_stereo_views();
    incremental.get_total_error();

    int added_count=0;
    for (std::size_t i = 0; i < viewports.size(); ++i){
        if(viewports[i].added)
            added_count++;
    }
    std::cout<< "Added :"<<added_count<<"/"<<viewports.size()<<std::endl;
    std::cout << "SfM reconstruction took " << timer.get_elapsed()
        << " ms." << std::endl;
    log_message(conf, "SfM reconstruction took "
        + util::string::get(timer.get_elapsed()) + "ms.");

    /* Normalize scene if requested. */
    if (conf.normalize_scene)
    {
        std::cout << "Normalizing scene..." << std::endl;
        incremental.normalize_scene();
    }


    for (std::size_t i = 0; i < viewports.size(); ++i){
        if(viewports[i].added){
        }
    }



    /* Save bundle file to scene. */
    std::cout << "Creating bundle data structure..." << std::endl;
    mve::Bundle::Ptr bundle = incremental.create_bundle();
    mve::save_mve_bundle(bundle, scene->get_path() + "/synth_0.out");

    /* Apply bundle cameras to views. */
    mve::Bundle::Cameras const& bundle_cams = bundle->get_cameras();
    mve::Scene::ViewList const& views = scene->get_views();
    std::size_t         stereo_view_count=0;
    for (std::size_t i = 0; i <views.size(); ++i)
    {
        mve::View::Ptr view = views[i];

        mve::ByteImage::Ptr original
        = view->get_byte_image(conf.original_name);
        if (original == nullptr)
            break;
        stereo_view_count++;
    }
    if (bundle_cams.size() != stereo_view_count * (conf.add_fixed_stereo_views ? 2 : 1) )
    {
        std::cerr << "Error: Invalid number of cameras " << bundle_cams.size() << " "<<stereo_view_count * (conf.add_fixed_stereo_views ? 2 : 1) <<  std::endl;
        std::exit(EXIT_FAILURE);
    }

    int num_bundle_cam=bundle_cams.size()/(conf.add_fixed_stereo_views ? 2:1);
    int slave_view_cnt=views.size()+1;
#pragma omp parallel for schedule(dynamic,1)
    for (std::size_t i = 0; i <num_bundle_cam; ++i)
    {
        mve::View::Ptr view = views[i];
        mve::CameraInfo const& cam = bundle_cams[i];
        if (view == nullptr)
            continue;
        if (view->get_camera().flen == 0.0f && cam.flen == 0.0f)
            continue;

        view->set_camera(cam);
#pragma omp critical
        std::cout << "Camera " << i << " t:" << cam.trans[0] << " "<< cam.trans[1]<<" "<< cam.trans[2]<<std::endl;

        /* Undistort image. */
        if (!conf.undistorted_name.empty())
        {
            mve::ByteImage::Ptr original
                = view->get_byte_image(conf.original_name);
            if (original == nullptr)
                continue;
               mve::ByteImage::Ptr  out_master = mve::ByteImage::create(original->width(), original->height(), original->channels());
            cv::Mat wrapped_master(original->height(), original->width(),
                            original->channels() > 1 ? CV_8UC3:CV_8U,original->begin());
            cv::Mat wrapped_master_rect(out_master->height(), out_master->width(),
                            original->channels() > 1 ? CV_8UC3:CV_8U,out_master->begin());


            mve::ByteImage::Ptr slave
                = view->get_byte_image("slave");
            if (slave == nullptr)
                continue;
            mve::ByteImage::Ptr  out_slave = mve::ByteImage::create(slave->width(), slave->height(), slave->channels());
            cv::Mat wrapped_slave(slave->height(), slave->width(),
                            slave->channels() > 1 ? CV_8UC3:CV_8U,slave->begin());
            cv::Mat wrapped_slave_rect(out_slave->height(), out_slave->width(),
                            out_slave->channels() > 1 ? CV_8UC3:CV_8U,out_slave->begin());
            viewports[0].stereo_calibration.rect_images(wrapped_master,wrapped_master_rect,wrapped_slave,wrapped_slave_rect);

            view->set_image(out_master, conf.undistorted_name);
            view->set_image(out_slave, "undistorted-slave");
            if(conf.add_fixed_stereo_views){
                int slave_id=(i+num_bundle_cam);
                std::string mve_fname = make_image_name(slave_id);
                mve::View::Ptr view_slave = mve::View::create();
                view_slave->set_id(slave_view_cnt);
                mve::CameraInfo const& cam_slave = bundle_cams[i+num_bundle_cam];
                view_slave->set_id(slave_id);
                view_slave->set_camera(cam_slave);
                view_slave->set_image(out_slave,conf.undistorted_name);
                view_slave->set_name((view->get_name()+"-slave"));
                #pragma omp critical
                std::cout << "Saving slave view " << util::fs::join_path(util::fs::dirname(view->get_directory()), mve_fname) << std::endl;
                view_slave->save_view_as(util::fs::join_path(util::fs::dirname(view->get_directory()), mve_fname));
                view_slave->cache_cleanup();
            }

        }

#pragma omp critical
        std::cout << "Saving view " << view->get_directory() << std::endl;
        view->save_view();
        view->cache_cleanup();
    }
    log_message(conf, "SfM reconstruction done.\n");
}

void
check_prebundle (AppSettings const& conf)
{
    std::string const prebundle_path
        = util::fs::join_path(conf.scene_path, conf.prebundle_file);

    if (util::fs::exists(prebundle_path.c_str()))
        return;

    /* Check if the prebundle is writable. */
    std::ofstream out(prebundle_path.c_str());
    if (!out.good())
    {
        out.close();
        std::cerr << "Error: Specified prebundle not writable: "
            << prebundle_path << std::endl;
        std::cerr << "Note: The prebundle is relative to the scene."
            << std::endl;
        std::exit(EXIT_FAILURE);
    }
    out.close();

    /* Looks good. Delete created prebundle. */
    util::fs::unlink(prebundle_path.c_str());
}

int
main (int argc, char** argv)
{
    util::system::register_segfault_handler();
    util::system::print_build_timestamp("MVE SfM Reconstruction");

    /* Setup argument parser. */
    util::Arguments args;
    args.set_usage(argv[0], "[ OPTIONS ] SCENE");
    args.set_exit_on_error(true);
    args.set_nonopt_maxnum(1);
    args.set_nonopt_minnum(1);
    args.set_helptext_indent(23);
    args.set_description("Reconstruction of camera parameters "
        "for MVE scenes using Structure from Motion. Note: The "
        "prebundle and the log file are relative to the scene directory.");
    args.add_option('o', "original", true, "Original image embedding [original]");
    args.add_option('e', "exif", true, "EXIF data embedding [exif]");
    args.add_option('m', "max-pixels", true, "Limit image size by iterative half-sizing [6000000]");
    args.add_option('u', "undistorted", true, "Undistorted image embedding [undistorted]");
    args.add_option('\0', "prebundle", true, "Load/store pre-bundle file [prebundle.sfm]");
    args.add_option('\0', "log-file", true, "Log some timings to file []");
    args.add_option('\0', "no-prediction", false, "Disable matchability prediction");
    args.add_option('\0', "normalize", false, "Normalize scene after reconstruction");
    args.add_option('\0', "skip-sfm", false, "Compute prebundle, skip SfM reconstruction");
    args.add_option('\0', "always-full-ba", false, "Run full bundle adjustment after every view");
    args.add_option('\0', "video-matching", true, "Only match to ARG previous frames [0]");
    args.add_option('\0', "fixed-intrinsics", false, "Do not optimize camera intrinsics");
    args.add_option('\0', "no-shared-intrinsics", false, "Do not share intrinsics between all cameras");
    args.add_option('\0', "intrinsics-from-views", false, "Use intrinsics from MVE views [use EXIF]");
    args.add_option('\0', "track-error-thres", true, "Error threshold for new tracks [20]");
    args.add_option('\0', "fixed-stereo-views", false, "Add fixed baseline stereo views as images");
    args.add_option('\0', "track-thres-factor", true, "Error threshold factor for tracks [25]");
    args.add_option('\0', "use-2cam-tracks", false, "Triangulate tracks from only two cameras");
    args.add_option('\0', "initial-pair", true, "Manually specify initial pair IDs [-1,-1]");
    args.add_option('\0', "min-feat-thresh", true, "Min feature matches frame to frame [10]");
    args.add_option('\0', "max-epi-dist",true ,"epipolar dist left-right matches and frame2frame matches rej [2.0]");
    args.parse(argc, argv);

    /* Setup defaults. */
    AppSettings conf;
    conf.scene_path = args.get_nth_nonopt(0);
    conf.original_name = "original";
    conf.undistorted_name = "undistorted";
    conf.exif_name = "exif";
    conf.prebundle_file = "prebundle.sfm";
    conf.max_image_size = 6000000;
    conf.lowres_matching = false;
    conf.normalize_scene = false;
    conf.skip_sfm = false;
    conf.always_full_ba = false;
    conf.video_matching = 0;
    conf.fixed_intrinsics = false;
    conf.shared_intrinsics = false;
    conf.intrinsics_from_views = false;
    conf.track_error_thres_factor = 25.0f;
    conf.new_track_error_thres = 20.0f;
    conf.min_views_per_track = 3;
    conf.initial_pair_1 = -1;
    conf.initial_pair_2 = -1;
    conf.min_feat_thresh = 10;
    conf.add_fixed_stereo_views=false;
    conf.max_epipolar_distance = 2.0;

    /* Read arguments. */
    for (util::ArgResult const* i = args.next_option();
        i != nullptr; i = args.next_option())
    {
        if (i->opt->lopt == "original")
            conf.original_name = i->arg;
        else if (i->opt->lopt == "exif")
            conf.exif_name = i->arg;
        else if (i->opt->lopt == "undistorted")
            conf.undistorted_name = i->arg;
        else if (i->opt->lopt == "max-pixels")
            conf.max_image_size = i->get_arg<int>();
        else if (i->opt->lopt == "prebundle")
            conf.prebundle_file = i->arg;
        else if (i->opt->lopt == "log-file")
            conf.log_file = i->arg;
        else if (i->opt->lopt == "no-prediction")
            conf.lowres_matching = false;
        else if (i->opt->lopt == "normalize")
            conf.normalize_scene = true;
        else if (i->opt->lopt == "skip-sfm")
            conf.skip_sfm = true;
        else if (i->opt->lopt == "fixed-stereo-views")
            conf.add_fixed_stereo_views = true;
        else if (i->opt->lopt == "always-full-ba")
            conf.always_full_ba = true;
        else if (i->opt->lopt == "video-matching")
            conf.video_matching = i->get_arg<int>();
        else if (i->opt->lopt == "fixed-intrinsics")
            conf.fixed_intrinsics = true;
        else if (i->opt->lopt == "no-shared-intrinsics")
            conf.shared_intrinsics = false;
        else if (i->opt->lopt == "intrinsics-from-views")
            conf.intrinsics_from_views = true;
        else if (i->opt->lopt == "track-error-thres")
            conf.new_track_error_thres = i->get_arg<float>();
        else if (i->opt->lopt == "max-epi-dist")
            conf.max_epipolar_distance = i->get_arg<float>();
        else if (i->opt->lopt == "track-thres-factor")
            conf.track_error_thres_factor = i->get_arg<float>();
        else if (i->opt->lopt == "min-feat-thresh")
            conf.min_feat_thresh = i->get_arg<int>();
        else if (i->opt->lopt == "use-2cam-tracks")
            conf.min_views_per_track = 2;
        else if (i->opt->lopt == "initial-pair")
        {
            util::Tokenizer tok;
            tok.split(i->arg, ',');
            if (tok.size() != 2)
            {
                std::cerr << "Error: Cannot parse initial pair." << std::endl;
                std::exit(EXIT_FAILURE);
            }
            conf.initial_pair_1 = tok.get_as<int>(0);
            conf.initial_pair_2 = tok.get_as<int>(1);
            std::cout << "Using initial pair (" << conf.initial_pair_1
                << "," << conf.initial_pair_2 << ")." << std::endl;
        }
        else
        {
            std::cerr << "Error: Unexpected option: "
                << i->opt->lopt << std::endl;
            std::exit(EXIT_FAILURE);
        }
    }

    try
    {
        check_prebundle(conf);
        sfm_reconstruct(conf);
    }
    catch (std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        std::exit(EXIT_FAILURE);
    }

    return EXIT_SUCCESS;
}
