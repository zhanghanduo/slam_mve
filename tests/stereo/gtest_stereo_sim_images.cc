// Test cases for perspective 3 point algorithms.
// Written by Simon Fuhrmann.

#include <gtest/gtest.h>

#include <vector>
#include "mve/bundle_io.h"

#include "math/vector.h"
#include "math/matrix.h"
#include "math/quaternion.h"
#include "sfm/camera_pose.h"
#include "sfm/fundamental.h"
#include "sfm/ransac.h"
#include "sfm/ransac_fundamental.h"
#include "sfm/triangulate.h"
#include "stereo/stereo_features.h"
#include "stereo/ransac_stereo_pose_rigid.h"
#include "stereo/pose_stereo_rigid.h"
#include "test_with_data_settings.h"
#include "util/string.h"
#include "util/file_system.h"
#include "util/tokenizer.h"
#include "stereo/stereo_filename_utils.h"
#include "mve/image.h"
#include "mve/image_io.h"
#include "sfm/nearest_neighbor.h"
#include "sfm/feature_set.h"
#include "sfm/bundler_common.h"
#include "sfm/bundler_features.h"
#include "sfm/bundler_matching.h"
#include "sfm/bundler_tracks.h"
#include "sfm/bundler_init_pair.h"
#include "sfm/bundler_intrinsics.h"
#include "sfm/bundler_incremental.h"
#include "stereo/stereo_features.h"
#include "stereo/stereo_matching.h"
#include "stereo/stereo_incremental.h"
#include "stereo/stereo_init_pair.h"
#include "util/exception.h"
#include "stereo/stereo_common.h"
#include "stereo/stereo_common.h"
#include "stereo/stereo_matching.h"
using namespace std;
namespace
{
math::Matrix4d load_gt_pose(std::string filename){
    fstream fin(filename.c_str());
       string line;
       int row=0;
       math::Matrix4d trans;
       while(getline(fin, line))
       {
           //the following line trims white space from the beginning of the string
           line.erase(line.begin(), find_if(line.begin(), line.end(), not1(ptr_fun<int, int>(isspace))));

           if(line[0] == '#') continue;

           if(stringstream(line) >> trans(row,0) >> trans(row,1) >> trans(row,2) >>trans(row,3))
            row++;
           else{
              std::cerr << "Failed to load GT trans correctly test will fail\n";
               exit(-1);
           }
       }
       return trans;
}

void
fill_ground_truth_pose (sfm::CameraPose* pose1_master)
{
    std::ifstream strm(TEST_DATA_CALIB_SIM);
    if(!strm.good()){
        std::cout <<"Can't load "<<TEST_DATA_CALIB_SIM << "\n";
        assert(0);
    }
    Stereo_Calib_CV  stereo_calibration;
    stereo_calibration.load_from_file(strm);
    //tx=stereo_calibration.T_mat(0);
    // Calibration with focal lenght 1 and 800x600 image.
    // The first camera looks straight along the z axis.
    if (pose1_master != NULL)
    {

       // pose1_master->set_k_matrix(800, 800 / 2, 600 / 2);
        pose1_master->K=stereo_calibration.leftCamRect->k_matrix;
        math::matrix_set_identity(*pose1_master->R, 3);
        pose1_master->t.fill(0.0);
        pose1_master->t[1]=5.0;

        /*double const angle = -MATH_PI / 16;
        pose1_master->R(0,0) = std::cos(angle); pose1_master->R(0,2) = std::sin(angle);
        pose1_master->R(1,1) = 1.0;
        pose1_master->R(2,0) = -std::sin(angle); pose1_master->R(2,2) = std::cos(angle);
*/
     //   *pose1_slave=*pose1_master;
//        pose1_slave->t[0]+=tx;

    }

    // The second camera is at (1,0.25,0) and rotated 45deg to the left.
 /*   if (pose2_master != NULL)
    {
        pose2_master->K=stereo_calibration.leftCamRect->k_matrix;

       // pose2_master->set_k_matrix(800, 800 / 2, 600 / 2);
       // pose2_master->R.fill(0.0);
        math::matrix_set_identity(*pose2_master->R, 3);

        double const angle = MATH_PI / 4;
        pose2_master->R(0,0) = std::cos(angle); pose2_master->R(0,2) = std::sin(angle);
        pose2_master->R(1,1) = 1.0;
        pose2_master->R(2,0) = -std::sin(angle); pose2_master->R(2,2) = std::cos(angle);

        pose2_master->t.fill(0.0); //pose2_master->t[1] = -2.0;//pose2_master->t[1] = 0.25;
        //pose2_master->t = pose2_master->R * -pose2_master->t;
        *pose2_slave=*pose2_master;
        pose2_slave->t[0]+=tx;

    }*/
}
void
fill_eight_random_points (std::vector<math::Vec3d>* points)
{
    points->push_back(math::Vec3d(-0.31, -0.42, 1.41));
    points->push_back(math::Vec3d( 0.34,  0.31, 6.82));
    points->push_back(math::Vec3d(-0.25, -0.24, 1.25));
    points->push_back(math::Vec3d( 0.47,  0.22, 0.66));
    points->push_back(math::Vec3d( 0.13,  0.03, 0.89));
    points->push_back(math::Vec3d(-0.13, -0.46, 1.15));
    points->push_back(math::Vec3d( 0.21, -0.23, 1.33));
    points->push_back(math::Vec3d(-0.42,  0.38, 0.62));/*
    for(int i =0 ;i <  10; i ++){
        for(int j=0; j < 10; j++){
                points->push_back(math::Vec3d(-0.1*i,  0.1*j, 0.2+(i*j)));
        }
    }*/
}
std::string
remove_file_extension (std::string const& filename)
{
    std::size_t pos = filename.find_last_of('.');
    if (pos != std::string::npos)
        return filename.substr(0, pos);
    return filename;
}
}
TEST(StereoRelPose, SyntheticPoseTest1)
{
    std::vector<math::Vec3d> cam_pos_gt;

    /*char fname[1024];
    std::map<std::pair<int,int>,math::Matrix4d> gt_mats;
    int num_frames=100;
    for(int i=0; i < num_frames; i++){
        sprintf(fname,"%s/from_%03d_to_%03d.txt",TEST_DATA_DIR,i,i+1);
        gt_mats.insert(make_pair(make_pair(i,i+1),load_gt_pose(fname)));
    }
    std::vector<math::Matrix4d> poses;
    std::vector<math::Matrix4d> calc_poses;

    for(auto const& trans: gt_mats){
        cout <<trans.first.first << "-" <<trans.first.second<<": " << trans.second<<endl;
    }

    for(auto const& trans: gt_mats){
        if(poses.empty())
            poses.push_back(trans.second);
        else
            poses.push_back(trans.second*poses.back());

    }*/
   /* util::fs::Directory dir;
    try { dir.scan(TEST_DATA_DIR); }
    catch (std::exception& e)
    {
        std::cerr << "Error scanning input dir: " << e.what() << std::endl;
        std::exit(EXIT_FAILURE);
    }
    std::sort(dir.begin(), dir.end());
    util::fs::Directory master_dir,slave_dir;
    ExpandWildCard(dir,master_dir,"c0");
    ExpandWildCard(dir,slave_dir,"c1");
    std::vector<std::pair<util::fs::File,util::fs::File> > pairs;
    RemoveNonPairs(master_dir,slave_dir,"c0","c1",0,pairs);
   */
    int id_cnt = 1;
    unsigned int loaded_ok=0;
    std::vector<sfm::CameraPose>cam_poses;
    sfm::CameraPose pose1_master;//,pose1_slave, pose2_master,pose2_slave;
    double tx; //Baseline for stereo pair

    fill_ground_truth_pose(&pose1_master);///&pose1_slave, &pose2_master,&pose2_slave,tx);

    sfm::CameraPose p=pose1_master;
    cam_poses.push_back(p);

    /*sfm::CameraPose p2=pose2_master;
    cam_poses.push_back(p2);

    sfm::CameraPose p3=pose2_master;
    p3.t[1]-=2.0;

    cam_poses.push_back(p3);


    sfm::CameraPose p4=p3;
    p4.t[1]-=7.0;

    cam_poses.push_back(p4);*/
    for(int i=0; i < 5; i ++){
        sfm::CameraPose pn=p;
        pn.t[1]-=1.0*(i+1);

        double const angle = (MATH_PI / 8)*(i %2);
        pn.R(0,0) = std::cos(angle); pn.R(0,2) = std::sin(angle);
        pn.R(1,1) = 1.0;
        pn.R(2,0) = (-std::sin(angle)); pn.R(2,2) = std::cos(angle);
        sfm::bundler::stereo::  roundprint(pn.R);
        cam_poses.push_back(pn);
    }
    for(int i=0; i < cam_poses.size(); i++){
        printf("GT Pose %i:",i);
        sfm::bundler::stereo::print_camera_position(cam_poses[i]);
        mve::CameraInfo cam;
        std::copy(cam_poses[i].R.begin(), cam_poses[i].R.end(), cam.rot);
        std::copy(cam_poses[i].t.begin(), cam_poses[i].t.end(), cam.trans);

        float pos[3];
        cam.fill_camera_pos(pos);
        cam_pos_gt.push_back(math::Vec3d(pos[0],pos[1],pos[2]));
    }

    int num_viewports=cam_poses.size();

    /*for(int i=0; i < num_viewports; i++){
        sfm::CameraPose p=pose1_master;
        p.t[1]-=2.0*i;
        cam_poses.push_back(p);
    }*/
#if 1
     mve::Scene::Ptr scene= mve::Scene::create();
    for (int i=0; i < num_viewports; i++)
    {

      /*  if (p.first.is_dir || p.second.is_dir)
        {
            std::cout << "Skipping directory " << p.first.name << std::endl;
            continue;
        }

        std::string fname_master = p.first.name;
        std::string afname_master = p.first.get_absolute_name();

//        std::cout << "Importing image " << fname_master << "..." << std::endl;
        mve::ImageBase::Ptr image_master = mve::image::load_file(afname_master);
        if (image_master == NULL)
            continue;


        std::string fname_slave = p.second.name;
        std::string afname_slave = p.second.get_absolute_name();

        mve::ImageBase::Ptr image_slave = mve::image::load_file(afname_slave);
        if (image_slave == NULL)
            continue;*/

        /* Create view, set headers, add image. */
        mve::View::Ptr view = mve::View::create();
        view->set_id(id_cnt);
     /*   view->set_name(remove_file_extension(fname_master));
        view->set_image_ref(afname_master, "original");
        view->set_image_ref(afname_slave, "slave");*/
        std::vector<char> calib_data;
        string stereo_calib;
        stereo_calib=string(TEST_DATA_CALIB_SIM);
        ReadAllBytes(stereo_calib.c_str(),calib_data);
        if(calib_data.size() == 0){
            std::cout <<"Can't load stereo calib file:" <<stereo_calib << std::endl;
            throw std::invalid_argument("Calib file invaid");
        }

        mve::ByteImage::Ptr calib_data_image = mve::ByteImage::create(calib_data.size(), 1, 1);
        std::copy(calib_data.begin(), calib_data.end(), calib_data_image->begin());
        view->set_blob(calib_data_image,  "stereocalib");
        scene->get_views().push_back(view);

        id_cnt += 1;
        loaded_ok += 1;

    }
  // scene->save_scene();
  // scene->save_views();

    //EXPECT_TRUE(loaded_ok == pairs.size());
#endif
    sfm::bundler::stereo::StereoViewportList *viewports = new  sfm::bundler::stereo::StereoViewportList ;
    sfm::bundler::stereo::StereoPairwiseMatching pairwise_matching;

    // Eight "random" 3D points.
    std::vector<math::Vec3d> points3d;
    fill_eight_random_points(&points3d);
    int num_3d_pts=points3d.size();


    viewports->resize(num_viewports);

    /* Read per-viewport data. */
    for (int i = 0; i < num_viewports; ++i)
    {

        //cout <<  "i: " <<cam_poses[i].t <<"\n" ;
        std::ifstream strm(TEST_DATA_CALIB_SIM);
        if(!strm.good()){
            std::cout <<"Can't load "<<TEST_DATA_CALIB_SIM << "\n";
            assert(0);
        }
        viewports->at(i).stereo_calibration.load_from_file(strm);


        viewports->at(i).pose.K=viewports->at(i).stereo_calibration.leftCamRect->k_matrix;
        viewports->at(i).focal_length=viewports->at(i).stereo_calibration.leftCamRect->fx();
        auto& vpf_m = viewports->at(i).features;
        auto& vpf_s = viewports->at(i).features_slave;

        /* Read positions. */
        int32_t num_positions=num_3d_pts;
        vpf_m.positions.resize(num_positions);
        vpf_s.positions.resize(num_positions);
        vpf_m.colors.resize(num_positions);
        vpf_s.colors.resize(num_positions);
        viewports->at(i).pts_3d.resize(num_positions);

        viewports->at(i).matches_master_slave.resize(num_positions);


        for (int j = 0; j < num_positions; ++j){
            math::Matrix<double, 4, 4>   world2cam = (cam_poses[i].R.hstack(cam_poses[i].t)).vstack(math::Vec4d(0,0,0,1.0));

            math::Vec3d local_point=world2cam.mult(points3d[j],1.0);// - cam_poses[i].t;
            viewports->at(i).pts_3d[j]=local_point;
      //      std::cout <<  "Orig: "<<viewports->at(i).pts_3d[j] << "\n";
            math::Vec3d xyd=sfm::bundler::stereo::cam_pt_to_rect_xyd( viewports->at(i).pose.K,viewports->at(i).stereo_calibration.T[0],local_point);
          //  std::cout << "XYD: "<<xyd<< std::endl;
           // std::cout <<  "re: "<<sfm::bundler::stereo::pix_uvd_to_xyz( viewports->at(i).pose.K,viewports->at(i).stereo_calibration.T[0],xyd) << "\n";

            vpf_m.positions[j]=math::Vec2f(xyd[0],xyd[1]);
            vpf_s.positions[j]=math::Vec2f(xyd[0]+xyd[2],xyd[1]);

            vpf_m.colors[j]=math::Vec3uc(255,255,255);
            vpf_s.colors[j]=math::Vec3uc(255,255,255);
            viewports->at(i).matches_master_slave[j]=j;

        }




    }



    /* Exhaustive matching between all pairs of views. */
    sfm::bundler::stereo::StereoMatching::Options matching_opts;
    //matching_opts.ransac_opts.max_iterations = 1000;
    //matching_opts.ransac_opts.threshold = 0.0015;
    matching_opts.ransac_opts.verbose_output = true;
    //matching_opts.use_lowres_matching = true;
    matching_opts.match_num_previous_frames = 1;//conf.video_matching;

    matching_opts.min_feature_matches=0;
        sfm::bundler::stereo::StereoMatching bundler_matching(matching_opts);

    /* Read number of matching pairs. */
    int32_t num_pairs=num_viewports-1;

    /* Read per-matching pair data. */
    for (int32_t i = 0; i < num_pairs; ++i)
    {
        int32_t id1, id2, num_matches=num_3d_pts;
        id1=i;
        id2=i+1;
         sfm::CorrespondenceIndices matches;
         std::stringstream message;
         sfm::Matching::Result matching_result;
        math::Matrix<double, 3, 4> relpose;
        matching_result.matches_1_2.resize(num_3d_pts);

        matching_result.matches_2_1.resize(num_3d_pts);
        for(int j=0; j< num_3d_pts; j++){
            matching_result.matches_1_2[j]=j;
            matching_result.matches_2_1[j]=j;

        }
          for(int j=0; j< num_3d_pts-2; j++){
              math::Matrix<double, 3, 4>  result;

            stereo::pose_stereo_rigid(viewports->at(id1).pts_3d[j],viewports->at(id1).pts_3d[j],viewports->at(id1).pts_3d[j],
                                      viewports->at(id2).pts_3d[j],viewports->at(id2).pts_3d[j],viewports->at(id2).pts_3d[j],result);
        //    std::cout <<"Rigid GT "<< result<<std::endl;

          }


        bundler_matching.two_view_matching_internal(  viewports->at(id1), viewports->at(id2),&matches,relpose,
                message,matching_result);
        //std::cout << message.str()<<std::endl;
        sfm::bundler::stereo::StereoTwoViewMatching tvr;
        tvr.view_1_id = static_cast<int>(id1);
        tvr.view_2_id = static_cast<int>(id2);
        tvr.matches.reserve(num_matches);
        tvr.relpose = relpose;
        //std::cout << "RelPose :" << id1<< "-"<< id2<<"\n";
       // std::cout << relpose << std::endl;
        for (int32_t j = 0; j < num_3d_pts; ++j)
        {
            int32_t i1=j, i2=j;
            sfm::CorrespondenceIndex c;
            c.first = static_cast<int>(i1);
            c.second = static_cast<int>(i2);
            tvr.matches.push_back(c);
        }
        pairwise_matching.view_ids2_tvms_idx[std::make_pair(id1,id2)]=pairwise_matching.tvms.size();
        pairwise_matching.tvms.push_back(tvr);
    }

    int error_count=0;

    sfm::bundler::ViewportList mono_viewports;

    for(auto &viewport : *viewports){

        viewport.focal_length = viewport.stereo_calibration.leftCamRect->fx();
        viewport.radial_distortion = 0.0f;
        sfm::bundler::Viewport mono_viewport=viewport;
        mono_viewports.push_back(mono_viewport);
    }



    /* Compute connected feature components, i.e. feature tracks. */
    sfm::bundler::TrackList tracks;
    {
        sfm::bundler::Tracks::Options tracks_options;
        tracks_options.verbose_output = true;

        sfm::bundler::Tracks bundler_tracks(tracks_options);
        std::cout << "Computing feature tracks..." << std::endl;
        sfm::bundler::PairwiseMatching mono_pairwise_matching;
        for(auto &pair : pairwise_matching.tvms){
            sfm::bundler::TwoViewMatching mono_tvm=pair;
            mono_pairwise_matching.push_back(mono_tvm);
        }
        bundler_tracks.compute(mono_pairwise_matching, &mono_viewports, &tracks);
        std::cout << "Created a total of " << tracks.size()
            << " tracks." << std::endl;
    }


    for(std::size_t i=0; i< mono_viewports.size(); i++){
        (*viewports)[i].track_ids=mono_viewports[i].track_ids;
    }
    /* Remove color data and pairwise matching to save memory. */
    for (std::size_t i = 0; i < viewports->size(); ++i)
        (*viewports)[i].features.colors.clear();

    /* Search for a good initial pair, or use the user-specified one. */
    sfm::bundler::stereo::InitialPair::Result init_pair_result;
    sfm::bundler::stereo::InitialPair::Options init_pair_opts;
    /*if (conf.initial_pair_1 < 0 || conf.initial_pair_2 < 0)
    {
        init_pair_opts.verbose_output = true;

        sfm::bundler::stereo::InitialPair init_pair(init_pair_opts);
        init_pair.initialize(viewports, tracks,pairwise_matching);
        init_pair.compute_pair(&init_pair_result);
    }
    else
    {*/

        sfm::bundler::stereo::InitialPair init_pair(init_pair_opts);
        init_pair.initialize(*viewports, tracks,pairwise_matching);
        init_pair.compute_pair(0,
            &init_pair_result);



    std::cout << "Using views " << init_pair_result.view_id
     //   << " and " << init_pair_result.view_2_id
        << " as initial pair." << std::endl;

    /* Incrementally compute full bundle. */
    sfm::bundler::stereo::Incremental::Options incremental_opts;
    //incremental_opts.pose_p3p_opts.max_iterations = 1000;
    //incremental_opts.pose_p3p_opts.threshold = 0.005f;
    incremental_opts.pose_p3p_opts.verbose_output = false;
  //  incremental_opts.track_error_threshold_factor = 1.0/conf.track_error_thres_factor;
   // incremental_opts.new_track_error_threshold = conf.new_track_error_thres;
    incremental_opts.min_triangulation_angle = MATH_DEG2RAD(1.0);
   // incremental_opts.ba_fixed_intrinsics = conf.fixed_intrinsics;
    //incremental_opts.ba_shared_intrinsics = conf.shared_intrinsics;
    incremental_opts.verbose_output = true;

    /* Initialize viewports with initial pair. */
    (*viewports)[init_pair_result.view_id].added = true;//init_pair_result.view_1_pose;

    (*viewports)[init_pair_result.view_id].pose = init_pair_result.view_pose;
//    (*viewports)[init_pair_result.view_2_id].pose = init_pair_result.view_2_pose;
 //   printf("Cam 1 pos ");
    sfm::bundler::stereo::print_camera_position((*viewports)[init_pair_result.view_id].pose );

 //   printf("Cam 2 pos ");
  //  sfm::bundler::stereo::print_camera_position((*viewports)[init_pair_result.view_2_id].pose);

    // std::cout << "Init Pair 1:\n" <<  (*viewports)[init_pair_result.view_2_id].pose.R << std::endl;
   // std::cout << "Init Pair 2:\n" <<  (*viewports)[init_pair_result.view_2_id].pose.t << std::endl;
    /* Initialize the incremental bundler and reconstruct first tracks. */
    sfm::bundler::stereo::Incremental incremental(incremental_opts);
    incremental.initialize(viewports, &tracks,&pairwise_matching);
//    incremental.set_last_view_id(init_pair_result.view_2_id);
    incremental.triangulate_new_tracks();
    incremental.invalidate_large_error_tracks(20.0);

    /* Run bundle adjustment. */
    std::cout << "Running full bundle adjustment..." << std::endl;
    incremental.bundle_adjustment_full();
    incremental.invalidate_large_error_tracks(2.0);

    /* Reconstruct remaining views. */
    int num_cameras_reconstructed = 2;
    int full_ba_num_skipped = 0;
    while (true)
    {
        /* Find suitable next views for reconstruction. */
        std::vector<int> next_views;
        incremental.find_next_views(&next_views);
        if (next_views.empty())
        {
            std::cout << "SfM reconstruction finished." << std::endl;
            break;
        }

        /* Reconstruct the next view. */
        int next_view_id = -1;
        for (std::size_t i = 0; i < next_views.size(); ++i)
        {
            std::cout << std::endl;
            std::cout << "Adding next view ID " << next_views[i]
                << " (" << (num_cameras_reconstructed + 1) << " of "
                << viewports->size() << ")..." << std::endl;
            if (incremental.reconstruct_next_view(next_views[i]))
            {
                std::cout  << "Added " <<next_views[i] << " OK\n";
                next_view_id = next_views[i];
                break;
            }
        }

        if (next_view_id < 0)
        {
            std::cout << "No valid next view. Exiting." << std::endl;
            break;
        }

        /* Run single-camera bundle adjustment. */
        std::cout << "Running single camera bundle adjustment..." << std::endl;
        incremental.bundle_adjustment_single_cam(next_view_id);
        incremental.triangulate_new_tracks();
        incremental.invalidate_large_error_tracks(20.0);
        num_cameras_reconstructed += 1;


            std::cout << "Running full bundle adjustment..." << std::endl;
            incremental.bundle_adjustment_full();
            full_ba_num_skipped = 0;

    }


    int added_count=0;
    for (std::size_t i = 0; i < viewports->size(); ++i){
       // std::cout << "KIL: "<<viewports->at(i).pose.t <<std::endl;
        if((*viewports)[i].added)
            added_count++;
    }
    /* Save bundle file to scene. */
    std::cout << "Creating bundle data structure..." << std::endl;
    mve::Bundle::Ptr bundle = incremental.create_bundle();
   //s mve::save_mve_bundle(bundle, scene->get_path() + "/synth_0.out");

    /* Apply bundle cameras to views. */
    mve::Bundle::Cameras const& bundle_cams = bundle->get_cameras();
    mve::Scene::ViewList const& views = scene->get_views();
    if (bundle_cams.size() != views.size())
    {
        std::cerr << "Error: Invalid number of cameras!" << std::endl;
        std::exit(EXIT_FAILURE);
    }

    for (std::size_t i = 0; i < bundle_cams.size(); ++i)
    {
        mve::View::Ptr view = views[i];
        mve::CameraInfo const& cam = bundle_cams[i];
        float cam_pos[3];
        cam.fill_camera_pos(cam_pos);
       // math::Vec3d cam_pos_v(cam_pos[0],cam_pos[1],cam_pos[2]);
       math::Vec3d cam_pos_v=  math::matrix_invert_trans(pose1_master.R.hstack(pose1_master.t).vstack(math::Vec4d(0,0,0,1.0))).delete_row(3) * math::Vec4d(cam_pos[0],cam_pos[1],cam_pos[2],1.0);

        if(!cam_pos_gt[i].is_similar(cam_pos_v,1e-5)){
            EXPECT_TRUE(cam_pos_gt[i].is_similar(cam_pos_v,1e-5));

            std::cout << "Failed GT: " << cam_pos_gt[i] << " computed: "<< cam_pos_v<<std::endl;
        }
    //    printf("%f %f %f\n",cam_pos[0],cam_pos[1],cam_pos[2]);
    }
   /* for(auto tvm: pairwise_matching.tvms){
        math::Matrix<double, 4, 4> calc=tvm.relpose.vstack(math::Vec4d(0,0,0,1.0));
        if(gt_mats.count(make_pair(tvm.view_2_id,tvm.view_id))){
            if(calc_poses.empty())
                calc_poses.push_back(calc);
            else
                calc_poses.push_back(calc*calc_poses.back());

            math::Matrix<double, 4, 4> gt=gt_mats[make_pair(tvm.view_2_id,tvm.view_id)];
                bool ok=calc.is_similar(gt,1e-1);
                EXPECT_TRUE(ok);

                if(!ok){
                    error_count++;
                    cout << "Matrix not the same GT:\n";
                    cout << gt <<endl << "Calc\n";
                    cout <<calc <<endl;

                }
        }

    }
    for(unsigned int i = 0; i < calc_poses.size(); i++){
        bool ok=calc_poses[i].is_similar(poses[i],1e-1);

        if(!ok){
            error_count++;
            cout << "Matrix not the same GT:\n";
            cout << poses[i] <<endl << "Calc\n";
            cout <<calc_poses[i] <<endl;

        }
    }

    printf("Error Count %d/%d\n",error_count,(int)pairwise_matching.tvms.size());
*/
}
