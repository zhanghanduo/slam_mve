// Test cases for perspective 3 point algorithms.
// Written by Simon Fuhrmann.

#include <gtest/gtest.h>

#include <vector>

#include "math/vector.h"
#include "math/matrix.h"
#include "math/quaternion.h"

#include "stereo/pose_stereo_rigid.h"
using namespace std;
namespace
{
    math::Matrix<double, 3, 4> get_pose(){
        math::Matrix<double, 3, 4> pose(0.0);
        double const angle = MATH_PI / 4.0f;
        math::Quaternion<double> quat(math::Vec3d(1.0,0.0,0.0),angle);
        math::Vec3d trans(4.0,16.0,-1.0);
        math::Matrix3d rot;
        quat.to_rotation_matrix(rot.begin());
        pose=rot.hstack(trans);
        return pose;
    }

    void
    fill_test_points (std::vector<math::Vec3d>* f1,
        std::vector<math::Vec3d>* f2)
    {

        math::Matrix<double, 3, 4> pose=get_pose();
        math::Vec3d p1(-1.0, 1.0, 2.0);
        math::Vec3d p2(0.0, 0.0, 3.0);
        math::Vec3d p3(1.0, 1.0, -2.0);

        f2->clear();
        f2->push_back(pose*math::Vec4d(p1,1.0));
        f2->push_back(pose*math::Vec4d(p2,1.0));
        f2->push_back(pose*math::Vec4d(p3,1.0));

        f1->clear();
        f1->push_back(p1);
        f1->push_back(p2);
        f1->push_back(p3);
    }

    void
    fill_groundtruth_data (
                           math::Matrix<double, 3, 4>* s)
    {   
        *s =get_pose();
    }
}


TEST(PoseRigidTest, GroundTruth1)
{
    std::vector<math::Vec3d> view1, view2;
    math::Matrix<double, 3, 4>  result;
    fill_test_points(&view1, &view2);

    stereo::pose_stereo_rigid(view1[0], view1[1], view1[2],
            view2[0], view2[1], view2[2], result);


    math::Matrix<double, 3, 4> solution;
    fill_groundtruth_data(&solution);


    bool found_good_solution = false;
    if (solution.is_similar(result, 1e-10))
        found_good_solution = true;

    EXPECT_TRUE(found_good_solution);
}
