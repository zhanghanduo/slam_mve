cmake_minimum_required(VERSION 2.8.12)
add_definitions(-funsafe-math-optimizations -fno-math-errno -Wall -Wextra -Wundef -pedantic -g -std=c++11)
find_package(OpenMP)
if(OPENMP_FOUND)
   set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
   set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
endif()
list(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake")
include(UpdateCacheVariable)

find_package(Threads REQUIRED)
find_package(Eigen3 REQUIRED)
find_package(PNG REQUIRED)
find_package(JPEG REQUIRED)
find_package(TIFF REQUIRED)
find_package(GTest)
find_package(OpenCV REQUIRED)
find_package(SuiteSparse REQUIRED)
find_package(Glog REQUIRED)
include_directories(${EIGEN3_INCLUDE_DIR})
include_directories(${PNG_INCLUDE_DIR})
include_directories(${JPEG_INCLUDE_DIR})
include_directories(${TIFF_INCLUDE_DIR})

include_directories(libs/ ../git/mve/libs/)
link_directories(${CMAKE_SOURCE_DIR}/../git/mve/libs/util
${CMAKE_SOURCE_DIR}/../git/mve/libs/sfm
${CMAKE_SOURCE_DIR}/../git/mve/libs/dmrecon
${CMAKE_SOURCE_DIR}/../git/mve/libs/mve)


subdirs(libs apps tests)
